const _ = require("lodash");
require('events').EventEmitter.prototype._maxListeners = 99999999999999;
const EventEmitter = require('events');
var myEmitter = new EventEmitter();
myEmitter.setMaxListeners(myEmitter.getMaxListeners() + 1);
myEmitter.once('event', () => {
    myEmitter.setMaxListeners(Math.max(self.myEmitter.getMaxListeners() - 1, 0));
});

/**
 * Function listen() is used to listen to an event and call a callback function when the event is emitted.
 * 
 * @param {string} event_name - The name of the event to listen to. The event name is prefixed with the value of process.env.EVENT_PREFIX_KEY.
 * @param {function} cb - The callback function to be executed when the event is emitted.
 * @param {EventEmitter} [event_Emitter=myEmitter] - The EventEmitter instance to use for event listening. If not provided, the default is myEmitter.
 */

module.exports.listen = function (event_name, cb, event_Emitter) {
    event_name = process.env.EVENT_PREFIX_KEY + "" + event_name;

    if (_.isUndefined(event_Emitter)) {
        event_Emitter = myEmitter
    }
    event_Emitter.addListener(event_name, function (args) {
        cb(args);
    });
}

/**
 * Emit an event.
 * @function
 * @param {string} event_name - The name of the event to be emitted.
 * @param {object} [args] - The arguments to pass to the event listeners.
 * @param {EventEmitter} [event_Emitter=myEmitter] - The event emitter instance to use. Defaults to the `myEmitter` global variable.
 */
module.exports.emit = function (event_name, args, event_Emitter) {
    event_name = process.env.EVENT_PREFIX_KEY + "" + event_name;
    if (_.isUndefined(event_Emitter)) {
        event_Emitter = myEmitter
    }

    if (!_.isUndefined(args)) {
        event_Emitter.emit(event_name, args);
    } else {
        event_Emitter.emit(event_name);
    }

}

/**
 * load_events()
 * @param {string} module_name - The name of the module to load events from. Module file must be created inside core folder or sub folder with this pattern 'file-name'.events.js
 * 
 * Loads events from a module.
 */

module.exports.load_events = async function (module_name) {
   /* if (parseInt(process.env.NODE_APP_INSTANCE) != 0) {
        return;
    }*/

    const { glob } = require('glob')

    let files_list = await glob(`././${module_name}/**/*.events.js`);

    for (let index = 0; index < files_list.length; index++) {
        const file = files_list[index];
        try {
            let dash = file.split("/");
            var full_path = "";
            var class_name = "";

            for (let i = 0; i < dash.length; i++) {
                const item = dash[i];
                var sup = "/";
                if (i == (dash.length - 1)) {
                    sup = "";
                }
                if (i == (dash.length - 2)) {
                    class_name = item;
                }
                if (item.toString() == ".") {
                    full_path += "../";
                } else {
                    full_path += item + sup;
                }
            }

            const path = require('path');
            let events_fn = require(path.resolve(full_path));
            _.each(events_fn, (item, index) => {
                item();
            });
        } catch (error) {
            console.error("Issue on load_events :", error);
        }
    }
}