const db_config = { 
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    options: {  
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        charset:'UTF8',
        dialectOptions: {
          useUTC: false, 
        },
        timezone:'+01:00'
    }
};
const DB = require("../../kamgoko/library/db");

const db_client = new DB(db_config);

module.exports = db_client;
