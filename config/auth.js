module.exports = {
     /** Configuration du cryptage et du décryptage des mots de passe avec la librairie cryptr */
     encrypt: {
        /** Ajouter un mot de passe fort */
        password: "",
        password_salt: 1
    },
    auth_clients: [
        {
            id: "app_id",
            secret: "app_secret",
            dev_secret: "app_dev_secret",
            domains: [],
            app_name: "App Name",
            role: "app_role"
        }
    ],
    roles_route_rules: {
        "app_role": {
            //Liste des méthodes autorisé pour les requêtes rpc. Si vous voulez autoriser toutes les méthodes, mettez juste *
            rpc: ["*"],
            //Liste des methodes autorisé les requêtes REST client. Si vous voulez autoriser toutes les méthodes, mettez juste *
            rest: [""]
        }
    },
    /** Origine des requêtes. Spécifiez la liste des domaines des applications sources. Exemple kamgoko.com, localhost:8098, 10.77.23.32   */
    cors_whitelist: [],
    /** Clé pour signer et vérifier une la clé JWT envoyé dans une requête REST. Ajouter une clé complexe */
    jwt: "",
    /** La clé à chercher dans Autorization dans l'entête des requêtes pour les requêtes privée */
    route_authorisation_key: "Bearer",
    /** 
     * Les différents type de requêtes autorisées sur l'application.
     * basic_auth: Signifie qu'il y aura des requêtes avec basic authorisation. Ces routes doivent être ajoutées dans router_rules dans le tableau basic_auth. Les informations d'identification seront les même que celles dans le tableau auth_clients ci dessous.
     * private: Signifie que pour certaines requêtes le client doit signé son entête avec une clé JWT à chercher dans le header de la requête. Les routes privées n'ont pas besoin d'être listées. Par défaut les routes sont privés si on ajouté private ici.
     * public: Signifie que certaines requêtes seront accessibles sans aucune forme d'authentification. Les routes public doivent être ajoutées dans le tableau public de router_rules.
     * 
     * Vous devez obligatoireement spécifier au moins un type selon le traitement de l'application
     * 
     * Dans l'odre de contrôle, on vérifie :
     * 1- Basic Authentification
     * 2- Public
     * 3- Private
     */
    route_secure_options: ["basic_auth", "private", "public"],
    /**
     * Par défaut toutes les routes pour chaque type de requête. Vous pouvez utiliser * pour autoriser des routes multiples. Exemple /public/* veut dire toutes les routes avec comme base * seront publiques. 
     */
    router_rules: {
        basicAuth: [],
        public: ["/public/*","/users/*"],
    }
};