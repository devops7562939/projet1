const config = require("../../config");
const { lang } = config;
const helpers = require("../helpers");
const _ = require("lodash");
var self = this;
const fs = require("fs");

/**
 * Adds error handling middleware to the Express app.
 *
 * @param {Object} app - Express app instance.
 */

module.exports.error_handler = function (app) {
    app.use((err, req, res, next) => {
        if (err) {
            if (err == 500) {
                err = lang.get('error_500');
            }
            helpers.events.emit(process.env.ERROR_EVENT_NAME, err);
            res.status(500).json(err);
        } else {
            next(5);
        }
    });
}

/**
 * Add middleware to the express app
 *
 * @param {Object} app - The express app object
 * @param {String} middleware_name - The name of the middleware to add
 * @param {Object} express - The express module
 */
module.exports.add_middleware = async function (app, middleware_name, express) {
    switch (middleware_name) {
        case "error_handler":
            this.error_handler(app);
            break;
        default:
            var fn = self[middleware_name];
            fn(app, express);
            break;
    }
}

/**
 * secure_routes - Middleware function to secure routes based on rules defined in the configuration auth file
 *
 * @param {object} app - Express app object
 * @param {object} config.auth - Auth configuration object containing the following properties:
 *  - route_secure_options {Array}: Array of security options to apply to routes (public, basic_auth, private)
 *  - router_rules {Object}: Rules is located in config/auth.js. Object containing routing rules, properties include:
 *    - basicAuth {Array}: Array of basic auth allowed routes, if first element is '*' all routes are allowed
 *    - public {Array}: Array of public routes, can include patterns with '*' for multiple routes with a common prefix
 *
 * @throws {Error} - If any error occurs while processing
 *
 * @returns {function} - Express middleware function to handle secure routing
 */
module.exports.secure_routes = async function (app) {
    app.use(async (req, res, next) => {
        var Route_rules = config.auth.router_rules;
        try {
            //Get current route path
            var base_path = req.originalUrl.split('?')[0];
            var current_path = base_path.trim();
            var last_slash = current_path.substr(current_path.length - 1);
            if (last_slash != "/") {
                current_path += "/";
            }

            if (_.includes(config.auth.route_secure_options, "basic_auth")) {
                //Check if current route is in allowed basic's authentification routes
                if (!_.isUndefined(Route_rules.basicAuth)) {
                    if (_.isArray(Route_rules.basicAuth) && !_.isEmpty(Route_rules.basicAuth)) {
                        if (_.includes(Route_rules.basicAuth, current_path) || Route_rules.basicAuth[0] == "*") {
                            var basic_infos = helpers.auth.get_basic_auth_infos(req);
                            var client = helpers.auth.check_rpc_or_rest_authorisation({ client_id: basic_infos.username, client_secret: basic_infos.password, route_or_module: current_path, req_type: "rest" });
                            if (client) {
                                req.kamgoko_client = client;
                                return next();
                            } else {
                                res.status(401);
                                return res.json(lang.get('unauthorized'));
                            }
                        }
                    }
                }
            }

            if (_.includes(config.auth.route_secure_options, "public")) {
                if (!_.isUndefined(Route_rules.public)) {
                    if (_.isArray(Route_rules.public) && !_.isEmpty(Route_rules.public)) {
                        if (_.includes(Route_rules.public, current_path)) {
                            return next();
                        } else {
                            //Check if the route url is inside a pattern
                            for (let index = 0; index < Route_rules.public.length; index++) {
                                const element = Route_rules.public[index];
                                if (element.includes("*")) {
                                    if (current_path.startsWith(element.substr(0, element.length - 2) + "/")) {
                                        return next();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (_.includes(config.auth.route_secure_options, "private")) {
                if (_.isUndefined(req.headers.authorization)) {
                    res.status(401);
                    return res.json(lang.get('unauthorized'));
                }

                var auth = req.headers['authorization'];
                var tmp = auth.split(' ');

                if (tmp.length != 2) {
                    res.status(401);
                    return res.json(lang.get('unauthorized'));
                }
                req.current_user = tmp[1];
                return next();
            }
            res.status(401);
            return res.json(lang.get('unauthorized'));
        } catch (error) {
            console.error(error);
            next(500);
        }
    });
}

/**
 * Start Express server.
 * 
 * @param {Object} app - Express app to be served
 * @param {Object} args - Object containing the following properties:
 *   @property {Number} port - Port number for the server to listen on
 *   @property {Object} [ssl_options] - (optional) Object containing the following properties:
 *     @property {String} key - Path to SSL private key file
 *     @property {String} cert - Path to SSL certificate file
 *     @property {String} ca - Path to SSL certificate authority file
 * 
 * @returns {void}
 */
module.exports.run_server = function (app, args) {
    try {
        if (_.isUndefined(args.ssl_options)) {
            return app.listen(args.port, function () {
                var log_name = process.env.APP_NAME + " express server loaded at port " + args.port;
                console.log(log_name.toUpperCase());
            });
        } else {
            var https = require('https');
            https.globalAgent.maxSockets = Infinity;
            var ssl_options = {
                key: fs.readFileSync(args.ssl_options.key),
                cert: fs.readFileSync(args.ssl_options.cert)
            };
            var server = https.createServer(ssl_options, app);
           return server.listen(args.port, function () {
                console.log(process.env.APP_NAME + " express server loaded at port " + args.port);
            });
        }
    } catch (error) {
        console.error(error);
        return false;
    }
}

/**
 * Loads a route for a given Express app.
 * 
 * @param {Object} app - Express app to load the route for
 * @param {String} route_path - Path to the route file. Route files must be create inside core folder
 * 
 * @returns {void}
 */
module.exports.load_route = function (app, route_path) {
    try {
        const path = require('path');
        const auth_routes = require(path.resolve(route_path));
        auth_routes.config(app);
    } catch (error) {
        console.error("Error on route including", error);
    }
}

/**
 * This function automatically loads the routes in a directory
 * @param {Object} app - Express app instance
 * @param {string} module_name - The name of the module to load the routes from. The module file must be create inside core folder of sub folder with this pattern 'name'.route.js
 */

module.exports.auto_load_routes = async function (app, module_name) {

    try {
        const {
            glob
        } = require('glob')
        var self = this;
        let files_list = await glob(`././${module_name}/**/*.routes.js`);

        for (let index = 0; index < files_list.length; index++) {
            const file = files_list[index];
            let dash = file.split("/");
            var full_path = "";

            for (let i = 0; i < dash.length; i++) {
                const item = dash[i];
                var sup = "/";
                if (i == (dash.length - 1)) {
                    sup = "";
                }

                if (item.toString() == ".") {
                    full_path += "../";
                } else {
                    full_path += item + sup;
                }
            }
            self.load_route(app, full_path);
        }
    } catch (error) {
        console.error("auto_load_routes : ", error);
    }
}