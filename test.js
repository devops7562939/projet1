const express = require('express');
const supertest = require('supertest');
const chai = require('chai');
const expect = chai.expect;
const asset = require('assert');

const app = require('./app'); 

const request = supertest(app);

describe('Test', () => {
  it('Return status 200 /', async () => {
    const response = await request.get('/public');
    expect(response.status).to.equal(200);
  });
});
