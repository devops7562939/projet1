var kamgoko = require("../../../kamgoko");
var eventManager=kamgoko.helpers.events;
let publicModel = require('./public.model');

exports.new_question = async () => {
    try {

        eventManager.listen("QUESTION_NEW", async function (question) {
            await publicModel.create({
                user_id: question.redactor_id,
                content: 'New Question is created',
                metas: question
            });
        });

    } catch (error) {
        console.log('Erreur lors de la sauvegarde du log',error.message);
    }
};
