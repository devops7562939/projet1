const { glob } = require('glob')
const _ = require('lodash');
const config = require("../../../config/");
const { lang, auth, base } = config;
const helper = require("../../helpers/")


/**
 * Autoload methods to expose en GRPC server
 * @param path Path of module to automaticly load
 * @returns list of methods per controller
 */
module.exports.load_files = async function (module_name) {
    try {
        var modules = {};
        for (let index = 0; index < module_name.length; index++) {
            const element = module_name[index];
            var file_list = await glob(`./././${element}/**/*.ctrls.js`);

            for (let index = 0; index < file_list.length; index++) {
                const file = file_list[index];
                try {
                    let dash = file.split("/");
                    var full_path = "";
                    var class_name = "";
                    dash.forEach(function (item, i) {
                        var sup = "/";
                        if (i == (dash.length - 1)) {
                            sup = "";
                        }
                        if (i == (dash.length - 2)) {
                            class_name = item;
                        }
                        if (item.toString() == ".") {
                            full_path += "../";
                        } else {
                            full_path += item + sup;
                        }
                    });
                    const path = require('path');
                    modules[class_name] = require(path.resolve(full_path));;
                } catch (error) {
                    console.error("Issue on glob.sync :", error);
                    return false;
                }
            }
        }
        return modules;

    } catch (error) {
        helper.events.emit(process.env.ERROR_EVENT_NAME, error);
        console.error("Error while loading GRPC files", error);
        return false;
    }

}

/**
 * define service
 *
 * @param {string} module_name - name of the module for which services are defined.
 *
 * @return {object} grpc_services - Object that defines the gRPC services.
 */


module.exports.define_services = async function (module_name) {
    var modules = await this.load_files(module_name);
    var grpc_services = {
        send: async (req, callback) => {
            try {
                var args_ = {
                    client_id: helper.auth.get_rpc_metadata(req, "x-api-key"),
                    client_secret: helper.auth.get_rpc_metadata(req, "x-api-secret"),
                    req_type: "rpc",
                    route_or_module: req.request.module
                };
                var current_client = helper.auth.check_rpc_or_rest_authorisation(args_);
                if (!current_client) {
                    return callback(null, {
                        code: 401,
                        success: 0,
                        response: JSON.stringify({ error: lang.get("unauthorized") }),
                    });
                }

                let args = JSON.parse(req.request.args);
                const get_class = req.request.module.split(/_(.+)/)[0];
                const controller_method = req.request.module.split(/_(.+)/)[1];


                if (_.isUndefined(modules[get_class.toLowerCase()])) {
                    return callback(null, {
                        code: 404,
                        success: 0,
                        response: lang.get("error_occured"),
                    });
                }
                if ( 
                    _.isUndefined(
                        modules[get_class.toLowerCase()][controller_method.toLowerCase()]
                    )
                ) {
                    return callback(null, {
                        code: 404,
                        success: 0,
                        response: lang.get("error_occured"),
                    });
                }
                const controller_method_function =
                    modules[get_class.toLowerCase()][controller_method.toLowerCase()];
                const resp = await controller_method_function({ body: args, query: args, kamgoko_client: current_client, is_rpc: true });

                callback(null, {
                    code: 200,
                    success: 1,
                    response: resp
                });
            } catch (error) {
                console.error(error);
                helper.events.emit(process.env.ERROR_EVENT_NAME, error);
                callback(null, {
                    code: 500,
                    success: 1,
                    response: lang.get("error_occured"),
                });
            }
        },
    };

    return grpc_services;
}

/**
 * Function to start a gRPC server for handling incoming requests
 * 
 * @param {string} module_name - The name of the module being loaded
 * @param {string} host - The host and port on which the server should bind
 * 
 * The server uses the following modules:
 * @module '@grpc/grpc-js' - gRPC implementation for Node.js
 * @module '@grpc/proto-loader' - Library for loading .proto files 
 * 
 * The server loads the main.proto file using the proto-loader module and 
 * adds the service defined in the define_services function. 
 * The server then binds to the specified host and port and starts listening for incoming requests. 
 * 
 * On success, a message is logged to the console indicating the server is loaded. 
 * On error, an error message is logged and emitted to a global error event. 
 */

module.exports.run = async function (module_name, host) {
    try {
        const grpc = require('@grpc/grpc-js');
        const protoLoader = require('@grpc/proto-loader');

        let proto_config = {
            keepCase: true,
            longs: String,
            enums: String,
            defaults: true,
            oneofs: true
        }

        let GlobalServicesProto = grpc.loadPackageDefinition(protoLoader.loadSync(__dirname + '/main.proto', proto_config))
        let server = new grpc.Server();
        server.addService(GlobalServicesProto.GlobalServices.service, await this.define_services(module_name));
        server.bindAsync(host, grpc.ServerCredentials.createInsecure(), () => server.start());
        var log_name = module_name + " GRPC SERVER LOADED AT " + host;
        console.log(log_name.toUpperCase());
        return true;
    } catch (error) {
        helper.events.emit(process.env.ERROR_EVENT_NAME, error);
        console.error("GRPC server not loaded", error);
        return false;
    }
}