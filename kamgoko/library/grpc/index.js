const Grpc_Server = require("./server");
const Grpc_Client = require("./client");

module.exports = { Grpc_Server, Grpc_Client };