var kamgoko = require("../../../kamgoko");
const { helpers } = kamgoko;


exports.index = async (req, res, next) => {

    try {

        let response = [{
            name: "Richard KAMGOKO",
            point: 20,
            rang: 1
        },{
            name: "Enfant Beni",
            point: 18,
            rang: 2
        }];
        return helpers.utils.return_response({
            global: {
                from: process.env.APP_NAME
            },
            response
        }, req, res);

    } catch (error) {
        console.log(error);
        return helpers.utils.return_response_error(req, next, error);
    }
}

exports.create = async (req, res, next) => {

    try {
        
        let response = {
            data:{
                name: 'Me and You',
                year: 1992
            }
        };
        return helpers.utils.return_response({
            success:true,
            global: {
                from: process.env.APP_NAME
            },
            response
        }, req, res);

    } catch (error) {
        console.log(error);

        return helpers.utils.return_response_error(req, next, error);
    }
}
