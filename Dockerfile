FROM node:20-alpine 

WORKDIR /app

COPY . .

RUN npm install

ENV APP_PORT=3100
EXPOSE 3100

CMD [ "node","app.js" ]



