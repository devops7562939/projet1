
const _ =require("lodash");

class KAMGOCORE_Redis {
    Redis = require("ioredis");
    sub = null;
    pub = null;
    prefix = "";
    /**
     * 
     * @param {JSON} config Configuration de redis {port?: number, host?: string, options:IORedis.RedisOptions}
     * @param {string} key_prefix Le prefix à ajouté sur le nom des channel. C'est idéal pour gérer plusieurs environnement avec la même instance de redis
     */
    constructor(config, key_prefix) {
        if (_.isUndefined(key_prefix)) {
            key_prefix = "";
        }
        this.sub = new this.Redis(config);
        this.pub = new this.Redis(config); 
        this.prefix = key_prefix;
    }

    /**
     * Publier dans dans un channel
     * @param {string} channel Le channel dans lequel on veut publier
     * @param {JSON} message Le message à publier
     * @returns 
     */
    async publish(channel, message) {
        try {
            return await this.pub.publish(this.prefix + "" + channel, JSON.stringify(message));
        } catch (e) {
            console.error("Redis Publish issue", e.stack);
            return false;
        }
    }

    /**
     * Écouter un événement dans redis
     * @param {String} my_channel Le channel à écouter ou le tableau de channel
     * @param {Function} cb La fonction callback qui prend comme argument (channel,message)
     */
    listen(my_channel, cb) {
        if(_.isArray(my_channel)){
            var tmp=[];
            for (let index = 0; index < my_channel.length; index++) {
                tmp[index]=this.prefix + "" + my_channel[index]
            }
            my_channel=tmp;
        }else{
            my_channel = [this.prefix + "" + my_channel];
        }
       
        try {
            this.sub.on("message", (channel, message) => {
                if (_.includes(my_channel,channel)) {
                    cb(JSON.parse(message),channel);
                }
            });
            this.sub.subscribe(my_channel);
        } catch (e) {
            console.error("Redis Listen issue", e.stack);
        }
    }

    /**
     * Ajouté un message dans redis
     * @param {String} key clé du message dans redis
     * @param {JSON} message contenu du message
     * @param {Number} duration durée en seconde
     * @returns 
     */
    async set(key, message, duration) {
        key = this.prefix + "" + key;
        try {
            await this.pub.set(key, JSON.stringify(message), "EX", duration);
            return true;
        } catch (e) {
            console.error("Redis set issue", e.stack);
            return false;
        }
    }

    /**
     * 
     * @param {String} key La clé du channel à récupérer 
     * @returns 
     */
    async get(key) {
        key = this.prefix + "" + key;
        try {
            return await this.pub.get(key);
        } catch (e) {
            console.error("Redis get issue", e.stack);
            return false;
        }
    }

    async raw_set(key, message, duration) {
        key = this.prefix + "" + key;
        try {
            await this.pub.set(key, JSON.stringify(message), "EX", duration);
            return true;
        } catch (e) {
            console.error("Redis set issue", e.stack);
            return false;
        }
    }

    async raw_get(key) {
        key = this.prefix + "" + key;
        try {
            return await this.pub.get(key); 
        } catch (e) {
            console.error("Redis get issue", e.stack);
            return false;
        }
    }
}
module.exports = KAMGOCORE_Redis;