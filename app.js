
var express = require('express');
var app = express();
async function start() {
    //REST 
    const KExpress = require("./kamgoko/library/express");

    var bodyParser = require('body-parser');
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true,
        limit: process.env.BODY_PARSER_JSON_LIMIT
    }));

  
    app.use(express.json());
       

    //Add route middleware
    KExpress.add_middleware(app, "secure_routes");

    //Load Routes 
    await KExpress.auto_load_routes(app, "core");

    KExpress.add_middleware(app, "error_handler");

    const helpers = require("./kamgoko/helpers");

    await helpers.events.load_events("core");
    
     KExpress.run_server(app, { port: process.env.APP_PORT });

}
start();

module.exports = app;

