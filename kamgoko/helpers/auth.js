const config = require("../../config/");
const _ = require("lodash");
var jwt = require('jsonwebtoken');
const kamgoko = require("../index.js");
var new_client_request = "NEW_CLIENT_REQUEST";

/**
 * check_jwt_token
 * @param {string} token - The JWT token to verify
 * @returns {object|boolean} decoded_infos - An object containing decoded information or false if token is invalid
 */

module.exports.check_jwt_token = function (token) {
    try {
        var decoded_infos = jwt.verify(token, config.auth.jwt);
        if (_.isObject(decoded_infos)) {
            return decoded_infos;
        }
        return false;
    } catch (error) {
        console.error(error);
        return false;
    }
};

/**
 * Get basic authentication information from the request headers
 *
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 *
 * @returns {Object} - Object containing `username` and `password` properties or `false` if not found
 */

module.exports.get_basic_auth_infos = function (req, res, next) {
    if (_.isUndefined(req.headers['authorization'])) {
        return false;
    } else {
        var auth = req.headers['authorization'];
        var tmp = auth.split(' ');

        var buf = Buffer.from(tmp[1], 'base64');
        var plain_auth = buf.toString();
        var creds = plain_auth.split(':');
        var username = creds[0];
        var password = creds[1];

        return { username: username, password: password };
    }
};

/**
 * Get RPC metadata from the request object
 * 
 * @param {Object} req - The request object
 * @param {string} key - The key of the metadata item to retrieve
 * @return {mixed} - The value of the metadata item or false if it doesn't exist
 */
module.exports.get_rpc_metadata = function (req, key) {
    if (_.isEmpty(req.metadata.get(key))) {
        return false;
    }
    return req.metadata.get(key)[0];
}

/**
 * Récupère l'adresse IP client
 * @function
 * 
 * @param {Object} req - Objet requête Express
 * 
 * @return {String} Adresse IP du client
 */

module.exports.get_client_IP = function (req) {
    return req.socket.remoteAddress;
}

/**
 * check_rpc_or_rest_authorisation - Check the authorization of the client to access a given route/module
 *
 * @param {object} args_data - An object containing the client_id, client_secret, route_or_module and req_type
 * @return {object|boolean} - The client data if authorized, false otherwise
 */
module.exports.check_rpc_or_rest_authorisation = function (args_data) {
    try {
        var { client_id, client_secret, route_or_module, req_type } = args_data;
        var index = _.findIndex(config.auth.auth_clients, function (o) { return o.id == client_id; });
        if (index >= 0) {
            var client = config.auth.auth_clients[index];
            if (client.secret.toString() == client_secret.toString()) {
                if (_.isUndefined(config.auth.roles_route_rules[client.role])) {
                    return false;
                }
                if (_.isUndefined(config.auth.roles_route_rules[client.role][req_type])) {
                    return false;
                }
                var roles_route_rules = config.auth.roles_route_rules[client.role][req_type];
                if (roles_route_rules[0] == "*") {
                    kamgoko.helpers.events.emit(new_client_request, args_data);
                    return client;
                }
                if (_.includes(roles_route_rules, route_or_module)) {
                    kamgoko.helpers.events.emit(new_client_request, args_data);
                    return client;
                }
            }
        }
        return false;
    } catch (error) {
        console.error("Error on authentification of RPC or REST", error);
    }
}

/**
 * Encrypts the given text.
 * 
 * @param {string} text - The text to be encrypted.
 * 
 * @returns {string|boolean} The encrypted text if successful, false otherwise.
 */

module.exports.encrypt = function (text) {
    try {
        const Cryptr = require('cryptr');
        const cryptr = new Cryptr(config.auth.encrypt.password);
        return cryptr.encrypt(text);
    } catch (error) {
        console.error(error);
        return false;
    }
};

/**
 * Decrypts encrypted text
 * @param {string} text - The text to decrypt
 * @return {string|boolean} Returns the decrypted text or false if an error occurs
 */
module.exports.unencrypt = function (text) {
    try {
        const Cryptr = require('cryptr');
        const cryptr = new Cryptr(config.auth.encrypt.password);
        return cryptr.decrypt(text);
    } catch (error) {
        console.error(error);
        return false;
    }
};
