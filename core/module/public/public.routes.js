const controller = require("./public.ctrls");
var base = "public";

module.exports.config = function (app) {
    app.get('/' + base, [
        controller.index
    ]);
    app.post('/' + base+'/create', [
        controller.create
    ]);
};