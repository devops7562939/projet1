const config = require("../../config/");
const _ = require("lodash");
const { v4: uuidv4 } = require('uuid');
const events=require("./events");

/**
 * 
 * @param {object} res - Express response object
 * @param {string} cookie_name - Name of the cookie
 * @param {string} cookie_value - Value of the cookie
 * @param {number} duration - Duration in minutes for which the cookie will be stored
 * 
 * @return {boolean} - Returns true if the cookie was set successfully, false otherwise
 */

module.exports.set_cookies = function (res, cookie_name, cookie_value, duration) {
    try {
        var options = {
            maxAge: 1000 * 60 * parseInt(duration),
            httpOnly: true,
            signed: false,
        }
        res.cookie(cookie_name, cookie_value, options);
        return true;
    } catch (error) {
        return false;
    }
};

/**
 * Get the cookie from the request
 * 
 * @param {Object} req - The request object
 * @param {string} cookie_name - The name of the cookie
 * 
 * @returns {(string|boolean)} - Returns the value of the cookie if it exists, returns false otherwise.
 */

module.exports.get_cookies = function (req, cookie_name) {
    try {
        if (_.isUndefined(req.cookies)) {
            false;
        }

        if (_.isUndefined(req.cookies[cookie_name])) {
            return false;
        }
        return req.cookies[cookie_name];
    } catch (error) {
        return false;
    }
};


/**
 * Control is an asynchronous function that executes a callback function
 * @param {Object} req - Express request object
 * @param {Object} res - Express response object
 * @param {Function} next - Express next middleware function
 * @param {Function} cb - The callback function to be executed, with req and res as its parameters
 */
module.exports.Control = async (req, res, next, cb) => {
    try {
        try {
            cb(req, res);
        } catch (error) {
            next("c");
            throw (e);
        }
    } catch (e) {
        next("c");
        throw (e);
    }
}

/**
 * Function to handle errors
 * @param {function} next - Express next middleware function
 * @param {Error} error - The error that was thrown
 */

module.exports.Control_error = async (next, error) => {
    next(500);
    throw (error);
}

/**
 * error_message
 * 
 * @returns {string} A string representing the error message.
 */

module.exports.error_message = async () => {
    return config.lang.get("error_500");
}

/**
 * Generates a unique identifier (UUID)
 * @returns {String} A universally unique identifier v4 string
 */

module.exports.getUUID = function () {
    return uuidv4();
};

/**
 * Generates a random number of specified length
 * @param {Number} length - The length of the desired number
 * @returns {String} - The generated random number
 */

 module.exports.generate_random_number=function(length) {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }


module.exports.remove_accents = function (strAccents) {
    var strAccents = strAccents.split('');
    var strAccentsOut = new Array();
    var strAccentsLen = strAccents.length;
    var accents = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž";
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    for (var y = 0; y < strAccentsLen; y++) {
        if (accents.indexOf(strAccents[y]) != -1) {
            strAccentsOut[y] = accentsOut.substr(accents.indexOf(strAccents[y]), 1);
        } else
            strAccentsOut[y] = strAccents[y];
    }
    strAccentsOut = strAccentsOut.join('');
    strAccentsOut = strAccentsOut.replace(/'/g, ' ');
    strAccentsOut = strAccentsOut.replace(/’/g, ' ');
    strAccentsOut = strAccentsOut.replace(/,/g, '');
    strAccentsOut = strAccentsOut.replace(/,/g, '');
    return strAccentsOut.toString();
}

module.exports.splitText = function (str, length) {
    return str.match(new RegExp('.{1,' + length + '}', 'g'));
}

module.exports.get_client_url = function (path) {
    try { 
        if (process.env.CLIENT_URL_USE_PORT) {
            return process.env.CLIENT_URL + "/" + path;
        }
        return process.env.CLIENT_URL;
    } catch (e) {
        console.log(e);
        return false;
    }
}


module.exports.random = function (min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
};

module.exports.get_random_element_from_array = function (array) {
    if (array.length == 0) {
        return false;
    }
    return array[this.random(0, (array.length - 1))];
};

module.exports.promisify = function (source) {
    const { promisify } = require('util');
    const to = {};
    for (var k in source) {
        if (typeof source[k] != 'function') continue;
        to[k] = promisify(source[k].bind(source));
    }
    return to;
}

/** Check if the req */
module.exports.is_rpc_request = function (req) {
    return (!_.isUndefined(req.rpc_client))
};

module.exports.return_response = function (data, req, res) {
    if (!_.isUndefined(req.is_rpc)) {
        return JSON.stringify(data);
    } else {
        return res.json(data);
    }
}; 

module.exports.return_response_error = function (req, next,error) {
    events.emit(process.env.ERROR_EVENT_NAME, error);
    if (_.isUndefined(req.is_rpc)) {
        return next(500);
    } else {
        return JSON.stringify(config.lang.get("error_500"));
    }
};

module.exports.is_url = (s, protocols) => {
    const { URL } = require('url');
    try {
        var url = new URL(s);
        return protocols
            ? url.protocol
                ? protocols.map(x => `${x.toLowerCase()}:`).includes(url.protocol)
                : false
            : true;
    } catch (err) {
        return false;
    }
};


module.exports.is_email = (mail) => {
    try {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return false;
    } catch (err) {
        return false;
    }
};


