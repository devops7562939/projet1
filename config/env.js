const dotenv = require("dotenv");
const envFile ='app.env'
dotenv.config({ path: envFile });
module.exports =process.env;