const _ = require("lodash");
const helpers = require("../helpers");

class KAMGOCORE_DB {
    Sequelize = require('sequelize');
    sequelize = false;
    db_config = {};

    /**
     * 
     * @param {JSON} db_config :
     * {
     * dialect:"",
     * port:"",
     * database:"",
     * host:"",
     * username:"",
     * db_password:""
     * }
     */
    constructor(db_config, can_connect) {
        try {
            if (_.isUndefined(can_connect)) {
                can_connect = true;
            }
            let db_config_default = {
                dialect: "postgresql",
                port: 5432,
                logging: false
            };
            var options = {
                ...db_config_default,
                ...db_config.options
            }
            db_config.options=options;
            this.db_config=db_config;
            if (_.isUndefined(db_config.username) || _.isUndefined(db_config.password) || _.isUndefined(db_config.options.host)) {
                console.error("DATABASE", "Missing Database Informations");
                return;
            }
            this.sequelize = new this.Sequelize(db_config.database, db_config.username, db_config.password, options);
            if (can_connect) {
                this.connect();
            }
            return this;
        } catch (error) {
            helpers.events.emit(process.env.ERROR_EVENT_NAME, error);
        }
    }

    async connect() {
        var self = this;
        try {
            this.sequelize
                .authenticate()
                .then(() => {
                    var log_name = self.db_config.database + ' database connection has been established successfully !';
                    console.log(log_name.toUpperCase());
                })
                .catch(err => {
                    console.error("DATABASE CONNECTION FAILED " + self.db_config.database, err);
                    setTimeout(function () {
                        self.connect();
                    }, 10000);
                });
        } catch (error) {
            helpers.events.emit(process.env.ERROR_EVENT_NAME, error);
            console.error("DATABASE CONNECTION FAILED " + self.db_config.database, err);
            setTimeout(function () {
                self.connect();
            }, 10000);
        }
    }

/*
 *Pour utiliser une transaction, il faut l'initialiser et le mettre ensuite dans les options
 * Execute query
 * @type string : The type of request INSERT | UPDATE | SELECT | DELETE
 * @sql object : SQL query
 * @params array : query parameters to be replace inside sql
 * @options : ORM custom options
 */
    async query(sql, params, options) {
        try {
            if (_.isUndefined(sql)) {
                console.error("Missed parameter $sql for query");
                return false;
            }

            if (_.isUndefined(options)) {
                var options = {};
            }

            if (_.isUndefined(params)) {
                var params = [];
            }

            var default_options = {
                raw: true,
                bind: params,
                logging: false
            };
            var rows = await this.sequelize.query(sql, _.merge(default_options, options));
            return rows[0];
        } catch (error) {
            helpers.events.emit(process.env.ERROR_EVENT_NAME, error);
            console.error(error);
        }
    };


    async get_new_transaction() {
        return await this.sequelize.transaction();
    }

    /*
     * Convert JSON to SQL
     * @type string : The type of request insert | update | select
     * @obj object : Object
     */
    object2SQL(obj, type, prefix) {
        try {
            if (_.isUndefined(type)) {
                type = 'insert';
            }

            if (_.isUndefined(prefix)) {
                prefix = "";
            }

            switch (type) {
                case "insert":
                    var object_keys = Object.keys(obj);
                    var sql_fields = object_keys.join(",", object_keys);
                    var sql_fields_template = "";

                    _.each(object_keys, function (value, key) {
                        var sup = ",";
                        if (key === 0) {
                            sup = "";
                        }
                        sql_fields_template += sup + "" + "$" + (key + 1);
                    });

                    return {
                        fields: sql_fields,
                        template: sql_fields_template,
                        values: Object.values(obj)
                    };
                    break;

                case "insertion":
                    var object_keys = Object.keys(obj.data);
                    var sql_fields = object_keys.join(",", object_keys);
                    var sql_fields_template = "";

                    _.each(object_keys, function (value, key) {
                        var sup = ",";
                        if (key === 0) {
                            sup = "";
                        }
                        sql_fields_template += sup + "" + "$" + (key + 1);
                    });

                    return {
                        sql: "INSERT INTO " + obj.table + " (" + sql_fields + ") VALUES (" + sql_fields_template + ") RETURNING *",
                        values: Object.values(obj.data)
                    };
                    break;

                case "update":
                    var global_values = Object.values(obj.new_values);
                    var new_values_keys = Object.keys(obj.new_values);
                    var set_string = "";
                    _.each(new_values_keys, function (key_value, key) {
                        var sup = ",";
                        if (key === 0) {
                            sup = "";
                        }
                        set_string += sup + "" + key_value + "=$" + (key + 1) + " ";
                    });

                    var where_string = "";
                    _.each(obj.where_values, function (key_value, key) {
                        var sup = obj.where_operator;
                        if (key === 0) {
                            sup = " ";
                        }
                        where_string += sup + " " + key_value.where + "" + key_value.operator + "$" + ((global_values.length) + 1) + " ";
                        global_values.push(key_value.val);
                    });

                    var more_sql = "";
                    if (!_.isUndefined(obj.more_sql)) {
                        if (obj.more_sql !== '') {
                            more_sql = obj.more_sql;
                        }
                    }

                    return {
                        sql: "UPDATE " + obj.table + " SET " + set_string + " WHERE " + where_string + "" + more_sql,
                        values: global_values
                    };
                    break;

                case "get_where":
                    var global_values = Object.values(obj);
                    var new_values_keys = Object.keys(obj);

                    var where_string = "";
                    _.each(new_values_keys, function (key_value, key) {
                        var sup = ",";
                        if (key === 0) {
                            sup = "";
                        }
                        set_string += sup + prefix + key_value + "=$" + (key + 1) + " ";
                    });

                    return {
                        sql: where_string,
                        values: global_values
                    };
                    break;
            }
        } catch (error) {
            helpers.events.emit(process.env.ERROR_EVENT_NAME, error);
            console.error(error);
        }
    }

}

module.exports = KAMGOCORE_DB; 