const tableName = 'question_table';
const _ = require('lodash');

exports.getAll = async (query, level, category_id, status, is_archived, page, limit) => {

  try {
    let redis_key = query+level+category_id+status+is_archived+page+limit;

    let sql = `SELECT * FROM questions WHERE 1=1 `;

    let values_array = [];

    let var_index = 1;

    if (query) {
      sql += `AND  content ILIKE $${var_index}`;
      values_array.push(`%${query}%`);
      var_index++;
    }

    if (category_id) {
      sql += ` AND category_id = $${var_index}`;
      values_array.push(category_id);
      var_index++;
    }

    if (level) {
      sql += ` AND level = $${var_index}`;
      values_array.push(level);
      var_index++;
    }

    if (status) {
      sql += ` AND status = $${var_index}`;
      values_array.push(status);
      var_index++;
    }
    if (is_archived) {
      sql += ` AND is_archived = var_index$${var_index}`;
      values_array.push(is_archived);
      var_index++;
    }


    values_array.push(limit);
    values_array.push((page - 1) * limit);

    sql += ` LIMIT $${var_index} OFFSET $${var_index + 1}`;

    let response =  await db_client.query(sql, values_array);

    return response;
  } catch (error) {
    console.log(error);
    return false;
  }

};

exports.getOne = async (data) => {
  try {
    let keys = Object.keys(data);
    let values = Object.values(data);

    let sql = `SELECT * FROM ${tableName} WHERE 1=1 `;

    keys.forEach((k, i) => {
      sql += ` AND ${k} = $${i + 1}`;
    });

    let questions = await db_client.query(sql, values);

    let first = _.first(questions);
    return first;

  } catch (error) {
    console.log(error);
    return false;
  }
};

exports.create = async (question,transaction) => {
  try {
    question = _.omitBy(question, _.isUndefined);

    var sql = db_client.object2SQL(question, "insert");


    var new_message = await db_client.query("INSERT INTO "+tableName+" (" + sql.fields + ") VALUES (" + sql.template + ") RETURNING *", sql.values,{
      transaction
    });
    //
    if(new_message){
      return _.first(new_message);
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};

exports.update_data = async (category, id) => {
  try {
    category = _.omitBy(category, _.isUndefined);
    var update_query = {
      table: tableName,
      operator: "AND",
      new_values: category,
      where_values: [
        {
          operator: "=",
          val: id,
          where: "id"
        }
      ],
      where_operator: "AND"
    };
    var update_query_sql = db_client.object2SQL(update_query, "update");

    let up = await db_client.query(update_query_sql.sql + " RETURNING *", update_query_sql.values);
    if(up){
      await cacheModule.delete();
    }
    return up;
  } catch (error) {
    console.log(error);
    return false;
  }
};

exports.deleteById = async (id) => {
  try {

    let ds = await db_client.query(`DELETE FROM ${tableName} WHERE id = $1`, [id]);
    
  } catch (error) {
    console.log(error);
    return false;
  }
};

