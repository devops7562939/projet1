const _ = require("lodash");
const env = require(__dirname + "/env");
const messages = {
    "fr": {
        login: "Connexion",
        cors_error: {intenal_error:"Une erreur s'est produite. Veuillez réssayer."},
        error_occured:"Une erreur s'est produite. Veuillez reéssayer.",
        error:{error:"Une erreur s'est produite. Veuillez reéssayer."},
        error_500:{internal_error:"Une erreur s'est produite. Veuillez réssayer."},
        error_404:{internal_error:"La ressource demandée n'existe."},
        unauthorized:"Vous n'êtes pas autorisé à accéder à cette ressource",
        phone_error:"Le N° de téléphone est incorrect",
        otp_message:"Votre code MyMTN Web est:",
        otp_code_expired:"Votre code OTP a expiré.",
        expired_session:"Votre session a expiré. Veuillez relancer",
        login_failed:"Mot de passe incorrect",
        user_exists:"Impossible de créer un compte avec ces informations",
        login_failed:"Connexion échoué",
        start_no_valid_field:"Veuillez entrer un numéro de téphone ou un email valide"
    }
}

exports.get = function (key, lang) {
    if (_.isUndefined(lang)) {
        lang = env.DEFAULT_LANG
    }
    if (!_.isUndefined(messages[lang][key])) {
        return messages[lang][key];
    }
    return "";
}