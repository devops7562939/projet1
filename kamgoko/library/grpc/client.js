const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
const helpers = require("../../helpers/");
const _ = require("lodash");
const config = require("../../../config");

module.exports = class Grpc_client {
    proto_config = {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        arrays: true,
        oneofs: true
    };
    constructor(server, proto_file_path) {
        this.server = server;
        this.proto_file_path = proto_file_path;
    }


    /**
     * Connect to gRPC server using specified connection details
     *
     * @param {String} this.proto_file_path - The path to the protobuf file 
     * @param {Object} this.proto_config - The configuration options for the protobuf file 
     * @param {String} this.server - The URL of the gRPC server 
     *
     * @returns {Promise} - A Promise that resolves to the gRPC connection instance 
     */

    connect() {
        var rpc_con = grpc.loadPackageDefinition(protoLoader.loadSync(this.proto_file_path, this.proto_config));
        return helpers.utils.promisify(new rpc_con.GlobalServices(this.server, grpc.credentials.createInsecure()));
    }

    /**
     * Call gRPC function
     *
     * @param {String} module - The module to be invoked 
     * @param {Object} args - The arguments to be passed to the module
     * @param {Array} [metadata_list] - An optional array of metadata to be passed with the request
     *   metadata_list [{
     *      key: string,
     *      value: string
     *   }]
     *
     * @returns {Promise} - A Promise that resolves to the response from the gRPC server 
     *   { error: string } if there was an error
    */

    async get(module, args, metadata_list) {

        if (_.isUndefined(module) || !_.isObject(args)) {
            return { error: config.lang.get("error_occured") };
        }
        try {
            const metadata = new grpc.Metadata();
            if (!_.isUndefined(metadata_list)) {
                if (_.isArray(metadata_list)) {
                    for (let index = 0; index < metadata_list.length; index++) {
                        const element = metadata_list[index];
                        if (_.isUndefined(element.value)) {
                            continue;
                        }
                        metadata.add(element.key, element.value);
                    }
                }
            }

            var client = this.connect();
            return await client.send({ module: module, args: JSON.stringify(args) }, metadata);
        } catch (error) {
            console.error("GRPC ERROR", error);
            return { error: config.lang.get("error_occured") };
        }
    }
}